import logging
import os

from config import config

slackKey = os.getenv("SLACK_API")
logging.info(slackKey)


# client = slack.WebClient(token=slackKey)


def sendSlackMessage(content):
    if config.postToSlack:
        # https://howchoo.com/python/python-send-slack-messages-slackclient
        logging.info("SLACK MESSAGE: " + content)
        try:
            #           client.chat_postMessage(channel='general', text=content)
            pass

        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            logging.info(message)
    else:
        logging.debug("Attempting to post to Slack, when slack is disabled: " + content)


def postGraphic(imageFile, fileTitle="Thermobot"):
    if config.postToSlack:
        # cairosvg.svg2png(url=graphFilepath, write_to=config.graphDirectory + destinationFilename)  # save new graph

        logging.info("POST SLACK IMAGE:" + imageFile)
        try:
        # client.files_upload(channels='general', file=imageFile, title=fileTitle)
            pass
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            logging.info(message)
    else:
        logging.debug("Attempting to post to Slack, when slack is disabled")
