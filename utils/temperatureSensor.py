import logging
import socket
from config import config
from flaskserver import app
if socket.gethostname() == config.targetHostname:
    import RPi.GPIO as GPIO
    import Adafruit_DHT


class temperatureSensor:

    def __init__(self):
        self.__lastTemperatureReading = 0
        self.__lastHumidityReading = 0
        GPIO.setmode(GPIO.BCM)
        self.DHT_SENSOR = Adafruit_DHT.DHT22
        self.DHT_PIN = 4
        self.initialReadingAfterInitialising = True
        self.maxTemperatureValueDrop = 0.75
        self.maxHumidityValueDrop = 2

    def getSensorData(self) -> (float, float, bool):
        sensorTemperature: float
        readSuccess: bool

        sensorHumidity, sensorTemperature = Adafruit_DHT.read_retry(self.DHT_SENSOR, self.DHT_PIN)

        if sensorHumidity is None or sensorTemperature is None:
            logging.debug("No sensor values returned")
            readSuccess = False
            sensorHumidity = 0
            sensorTemperature = 0
        else:
            logging.debug("Sensor values returned")
            sensorTemperature = sensorTemperature + config.temperatureCalibration

            if self.initialReadingAfterInitialising:
                self.initialReadingAfterInitialising = False
                readSuccess = True
            else:
                sensorHumidity, sensorTemperature, readSuccess = \
                    self.__sensorErrorCheck(sensorHumidity, sensorTemperature)

            self.__lastTemperatureReading = sensorTemperature
            self.__lastHumidityReading = sensorHumidity

        return sensorHumidity, sensorTemperature, readSuccess

    def __sensorErrorCheck(self, humidity: float, temperature: float) -> (float, float, bool):
        # Only allows a value to drop by a permitted amount
        # This resolves the issue where the sensor occasionally returns a valid looking, but erroneous, low value
        trustedReading = True

        if (self.__lastTemperatureReading - temperature) > self.maxTemperatureValueDrop:
            app.logger.debug(
                "Corrected error in temperature data: read " + str(temperature) + " when last reading was " + str(
                    self.__lastTemperatureReading))
            temperature = self.__lastTemperatureReading - self.maxTemperatureValueDrop

            trustedReading = False

        if (self.__lastHumidityReading - humidity) > self.maxHumidityValueDrop:
            app.logger.debug(
                "Corrected error in humidity data: read " + str(humidity) + " when last reading was " + str(
                    self.__lastHumidityReading))
            humidity = self.__lastHumidityReading - self.maxHumidityValueDrop

            trustedReading = False

        self.__lastTemperatureReading = temperature
        self.__lastHumidityReading = humidity

        return humidity, temperature, trustedReading
