import os

from config import config


class maxMinTracker:
    def __init__(self):
        self.awaitingFirstEntry = True
        self.maxTemp = None
        self.minTemp = None
        self.maxHumidity = None
        self.minHumidity = None

    def considerEntry(self, temperature, humidity):
        if self.awaitingFirstEntry:
            self.minTemp = self.maxTemp = temperature
            self.minHumidity = self.maxHumidity = humidity
            self.awaitingFirstEntry = False
        else:
            if temperature > self.maxTemp:
                self.maxTemp = temperature
            if temperature < self.minTemp:
                self.minTemp = temperature
            if humidity > self.maxHumidity:
                self.maxHumidity = humidity
            if humidity < self.minHumidity:
                self.minHumidity = humidity

    def getMinTemp(self):
        return self.minTemp

    def getMaxTemp(self):
        return self.maxTemp

    def getMinHumidity(self):
        return self.minHumidity

    def getMaxHumidity(self):
        return self.maxHumidity

    def reset(self):
        self.awaitingFirstEntry = True
