import os.path

from flaskserver import app


def safeDelete(file):
    try:
        os.remove(file)
    except:
        app.logger.error("Error removing " + file)


def copyFiles(source, destination):
    try:
        os.system("cp " + source + " " + destination)
    except:
        app.logger.error("Error copying " + source + " " + destination)
