import os

from config import config


class logger:
    def __init__(self, filename, maxEntries, initialise):
        self.filename = filename
        self.location = config.dataDirectory
        self.maxEntries = maxEntries
        self.FQFilename = self.location + filename
        if initialise and os.path.exists(self.FQFilename):
            os.remove(self.FQFilename)

    def addEntry(self, entry):
        with open(self.FQFilename, 'a+') as file:
            file.write(entry)
        self.__validateDataFile()

    def __validateDataFile(self):
        # check is a log has got too large, and if it has, remove oldest data element
        with open(self.FQFilename, 'r') as dateFile:
            if len(dateFile.readlines()) > self.maxEntries:
                self.__trimFile()

    def __trimFile(self):
        with open(self.FQFilename, 'r') as originalFile:
            data = originalFile.read().splitlines(True)
        with open(self.FQFilename, 'w') as trimmedFile:
            trimmedFile.writelines(data[1:])
