from random import uniform
from flaskserver import app


class temperatureSensorStub:

    def __init__(self):
        self.__lastTemperatureReading = 0
        self.__lastHumidityReading = 0
        self.maxTemperatureValueDrop = 0.75
        self.maxHumidityValueDrop = 2

    def getSensorData(self) -> (float, float, bool):
        sensorHumidity, sensorTemperature = self.__getRandomSensorReading(40.0, 60, 20, 30)
        sensorHumidity, sensorTemperature, readSuccess = self.__sensorErrorCheck(sensorHumidity, sensorTemperature)

        self.__lastTemperatureReading = sensorTemperature
        self.__lastHumidityReading = sensorHumidity

        return sensorHumidity, sensorTemperature, readSuccess

    @staticmethod
    def __getRandomSensorReading(tempMin: float, tempMax: float, humidityMin: float, humidityMax: float) -> (
            float, float):
        return round(uniform(tempMin, tempMax), 1), round(uniform(humidityMin, humidityMax), 1)

    def __sensorErrorCheck(self, humidity: float, temperature: float) -> (float, float, bool):
        # Only allows a value to drop by a permitted amount
        # This resolves the issue where the sensor occasionally returns a valid looking, but erroneous, low value
        trustedReading = True

        # if (self.__lastTemperatureReading - temperature) > self.maxTemperatureValueDrop:
        #     app.utils.debug(
        #         "Corrected error in temperature data: read " + str(temperature) + " when last reading was " + str(
        #             self.__lastTemperatureReading))
        #     temperature = self.__lastTemperatureReading - self.maxTemperatureValueDrop
        #
        #     trustedReading = False
        #
        # if (self.__lastHumidityReading - humidity) > self.maxHumidityValueDrop:
        #     app.utils.debug(
        #         "Corrected error in humidity data: read " + str(humidity) + " when last reading was " + str(
        #             self.__lastHumidityReading))
        #     humidity = self.__lastHumidityReading - self.maxHumidityValueDrop
        #
        #     trustedReading = False
        #
        # self.__lastTemperatureReading = temperature
        # self.__lastHumidityReading = humidity

        return humidity, temperature, trustedReading
