# thermometer data utils
from datetime import datetime
from datetime import timedelta

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import os

__dataDirectory = os.getcwd() + "/data/"


def plotTempMinMax(csvFile, filename):
    # read data

    headers = ['Date', 'Time', 'Temperature', 'Humidity']
    data = pd.read_csv(csvFile, names=headers, parse_dates=['Date'], dayfirst=True)

    dataMaxTemps = data.groupby('Date')['Temperature'].max().reset_index()
    dataMinTemps = data.groupby('Date')['Temperature'].min().reset_index()

    print(dataMaxTemps)
    print(dataMinTemps)

    dateSequence = dataMaxTemps['Date']
    minSequence = dataMinTemps['Temperature']
    maxSequence = dataMaxTemps['Temperature']

    # labels = dateSequence
    width = .8

    fig, ax = plt.subplots()

    fig.set_figwidth(8)
    fig.set_figheight(4)
    # get current axes (GCA) and set the y axis to match the data set
    plt.gca().set_ylim(dataMinTemps['Temperature'].min() - 2, dataMaxTemps['Temperature'].max() + 2)

    ax.bar(dateSequence, maxSequence, width, label='Max', align='center', color='r')
    ax.bar(dateSequence, minSequence, width, label='Min', align='center', color='w')

    ax.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')
    ax.set_title('Daily Temperature Range')

    ax.xaxis.set_major_formatter(dates.DateFormatter('%d %b'))
    ax.set_xlabel('Date')
    ax.xaxis.set_major_locator(dates.DayLocator(0, 1))

    # plt.tight_layout()
    plt.gcf().autofmt_xdate()
    plt.savefig(filename, dpi=250, pad_inches=1)

    plt.show()


def plotSeries(csvFile, timespan, dataSeries, xAxisLabel, dateTimeFormat, filename):
    startDate = (datetime.now() - timedelta(timespan)).strftime("%Y-%m-%d")

    headers = ['Date', 'Time', 'Temperature', 'Humidity']
    data = pd.read_csv(csvFile, names=headers, parse_dates=[['Date', 'Time']], dayfirst=True)

    dateMask = (data['Date_Time'] > startDate)

    data = data.loc[dateMask]

    timeSequence = data['Date_Time']
    tempSequence = data[dataSeries]

    fig, ax = plt.subplots()

    fig.set_figwidth(8)
    fig.set_figheight(4)

    if dataSeries == 'Temperature':
        ax.plot(timeSequence, tempSequence, '-r', color='r')
        ax.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')
        ax.set_xlabel(xAxisLabel)
    else:
        ax.plot(timeSequence, tempSequence, '-r', color='b')
        ax.set_ylabel('Humidity (%)')
        ax.set_xlabel(xAxisLabel)

    myFmt = dates.DateFormatter(dateTimeFormat)  # '%H:%M'
    ax.xaxis.set_major_formatter(myFmt)

    plt.gcf().autofmt_xdate()
    plt.savefig(filename, dpi=250, pad_inches=1)

    plt.show()


if __name__ == '__main__':
    plotMinMax("/home/john/development/Python/thermobot/test/test data.csv", "max min by day")

    plotTempRange("test data.csv",
                  0, "Temperature", "hh:mm", "%H:%M", "temperature today")
    plotTempRange("/home/john/development/Python/thermobot/test/test data.csv",
                  6, "Temperature", "dd/mm hour", '%d/%m %H', "temperature last_week")
    plotTempRange("/home/john/development/Python/thermobot/test/test data.csv",
                  1000, "Temperature", 'Date, Time (dd/mm hh)', '%d/%m %H', "temperature all_time")

    plotTempRange("test data.csv",
                  0, "Humidity", "hh:mm", "%H:%M", "humidity today")
    plotTempRange("/home/john/development/Python/thermobot/test/test data.csv",
                  6, "Humidity", "dd/mm hour", '%d/%m %H', "humidity last_week")
    plotTempRange("/home/john/development/Python/thermobot/test/test data.csv",
                  1000, "Humidity", 'Date, Time (dd/mm hh)', '%d/%m %H', "humidity all_time")
