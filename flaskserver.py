import os
from glob import glob

from flask import Flask, render_template

from config import config

app = Flask(__name__, template_folder=config.websiteContentDirectory)


def getLastValue(switch):
    if os.path.exists(config.lastValueDataFile):
        with open(config.lastValueDataFile, "r") as dataFile:
            dataLine = dataFile.readline()
            dataLine = dataLine.strip()
        dataFile.close()
        dataList = dataLine.split(',')
    else:
        dataList = ["01/01/2023", "00:00", "0.0", "0.0"]  # default values if file does not yet exist

    if switch == "Te":
        value = dataList[2]
    elif switch == "Hu":
        value = dataList[3]
    elif switch == "Ti":
        value = dataList[0] + ", " + dataList[1]
    return value


def getGraphFilename(baseFilename):
    fileList = (glob(config.graphDirectory + "*" + baseFilename))
    if len(fileList) == 0:
        result = "No graph file found"
    else:
        result = "static/graphs/" + os.path.basename(fileList[0])
    return result


@app.route("/")
def home():
    app.logger.debug("Homepage hit received")
    return render_template('temperature.html',
                           temperature=getLastValue("Te"),
                           humidity=getLastValue("Hu"),
                           timestamp=getLastValue("Ti"),
                           graphLiveTemperatureFilename=getGraphFilename(
                               config.temperatureLiveBaseFilename),
                           graphYesterdayTemperatureFilename=getGraphFilename(
                               config.temperatureYesterdayBaseFilename),
                           graphRecentTemperatureFilename=getGraphFilename(
                               config.temperatureRecentBaseFilename),
                           graphMaxMinTemperatureFilename=getGraphFilename(
                               config.temperatureMaxMinBaseFilename),

                           graphLiveHumidityFilename=getGraphFilename(
                               config.humidityLiveBaseFilename),
                           graphYesterdayHumidityFilename=getGraphFilename(
                               config.humidityYesterdayBaseFilename),
                           graphRecentHumidityFilename=getGraphFilename(
                               config.humidityRecentBaseFilename),
                           graphMaxMinHumidityFilename=getGraphFilename(
                               config.humidityMaxMinBaseFilename))


@app.route("/wife")
def humidity():
    app.logger.debug("Test page hit received")
    return render_template('wife.html',
                           temperature=getLastValue("Te"),
                           humidity=getLastValue("Hu"),
                           timestamp=getLastValue("Ti"),
                           graphLiveTemperatureFilename=getGraphFilename(
                               config.temperatureLiveBaseFilename),
                           graphYesterdayTemperatureFilename=getGraphFilename(
                               config.temperatureYesterdayBaseFilename),
                           graphRecentTemperatureFilename=getGraphFilename(
                               config.temperatureRecentBaseFilename),
                           graphMaxMinTemperatureFilename=getGraphFilename(
                               config.temperatureMaxMinBaseFilename))


def initialise(debugRequired):
    app.run(debug=debugRequired, host='0.0.0.0')


if __name__ == '__main__':
    app.logger.debug("Started webserver. Working directory: " + config.applicationDirectory)
    app.run(debug=True, host='0.0.0.0')
