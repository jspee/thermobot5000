# thermometer data utils

import glob
import logging
import os
import socket
import threading
from datetime import datetime, timedelta
from time import sleep

from config import config
from flaskserver import initialise, app
from graphing.graphGenerator import plotMaxMin, plotSeries
from messaging import slackbot
from utils.logger import logger
from utils.maxMinTracker import maxMinTracker
from utils.temperatureSensor import temperatureSensor
from utils.temperatureSensorStub import temperatureSensorStub
from utils.utilities import safeDelete, copyFiles

# setup logging
logging.basicConfig(format='%(asctime)s %(message)s')
handler = logging.FileHandler(config.dataDirectory + "thermobot.log")
app.logger.addHandler(handler)

# Deployment environment switch - running as debugMode stubs out all GPIO functions
if socket.gethostname() == config.targetHostname:
    pcMode = False
    from eInk.display import Display

    app.logger.setLevel(logging.ERROR)
else:
    pcMode = True
    app.logger.setLevel(logging.DEBUG)


def getTimestamp():
    return datetime.now().strftime("%Y-%m-%d_%H:%M:%S")


def getSensorReading(THsesor):
    return THsesor.getSensorData()


def logSensorReading(dataFile, temperature, humidity, dayOffset=0):
    if dayOffset == 0:
        dateTime = datetime.today()
    else:
        dateTime = datetime.today() + timedelta(days=dayOffset)

    date = dateTime.strftime('%d/%m/%y')
    time = dateTime.strftime('%H:%M:%S')
    fileLine = date + "," + time + "," + "{0:0.1f},{1:0.1f}".format(temperature, humidity) + "\n"
    dataFile.addEntry(fileLine)


def removeOldGraphFiles():
    # Get a list of all the files of graphType
    fileList = glob.glob(config.graphDirectory + "*.svg")
    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        try:
            os.remove(filePath)
        except Exception as ex:
            app.logger.error(ex)


def createTodayGraphSet(datafile):
    graphFilepathAndPrefix = config.graphDirectory + datetime.now().strftime("%d-%b-%Y-%H-%M-%S-")

    plotSeries(datafile, "Temperature", "hh:mm", "%H:%M",
               graphFilepathAndPrefix, config.temperatureLiveBaseFilename)
    plotSeries(datafile, "Humidity", "hh:mm", "%H:%M",
               graphFilepathAndPrefix, config.humidityLiveBaseFilename)


def createHistoricGraphSet(yesterdayData, recentData, maxMinData):
    graphFilepathAndPrefix = config.graphDirectory + datetime.now().strftime("%d-%b-%Y-%H-%M-%S-")
    # removeGraphType("svg")

    # yesterday data
    plotSeries(yesterdayData, "Temperature", "hr:min", "%H:%M",
               graphFilepathAndPrefix, config.temperatureYesterdayBaseFilename)
    plotSeries(yesterdayData, "Humidity", "hr:min", "%H:%M",
               graphFilepathAndPrefix, config.humidityYesterdayBaseFilename)
    # recent data
    plotSeries(recentData, "Temperature", "day/month", '%d/%m',
               graphFilepathAndPrefix, config.temperatureRecentBaseFilename)
    plotSeries(recentData, "Humidity", "day/month", '%d/%m',
               graphFilepathAndPrefix, config.humidityRecentBaseFilename)
    # max min data
    plotMaxMin(maxMinData, "Temperature",
               graphFilepathAndPrefix, config.temperatureMaxMinBaseFilename)
    plotMaxMin(maxMinData, "Humidity",
               graphFilepathAndPrefix, config.humidityMaxMinBaseFilename)


def setConfigToTestValues():
    config.dataSamplePeriodSeconds = config.testDataSamplePeriod
    config.updateGraphPeriodSeconds = config.testUpdateGraphPeriod
    config.updateDisplayPeriodSeconds = config.testUpdateDisplayPeriod
    config.slackPostPeriodSeconds = config.testSlackPostPeriod


def updateEInkWithTempHumidity():
    humidity, temperature, success = sensor.getSensorData()
    if success:
        try:
            display.updateDisplay(temperature, humidity)
        except Exception as ex:
            template = "An exception of type {0} occurred. No display found. Arguments:\n{1!r}"
            app.logger.info(template.format(type(ex).__name__, ex.args))


def updateSlackWithTempHumidity():
    humidity, temperature, success = sensor.getSensorData()
    if config.postToSlack:
        slackbot.sendSlackMessage(
            "Reading was {0:0.1f}C with {1:0.0f}% humidity.".format(temperature, humidity))


def runEndOfDayActivities(maxMinTracker, maxMinLog):
    # update max min for yesterday - two reading, the highest temp/humidity and the lowest temp and humidity
    logSensorReading(maxMinLog, maxMinTracker.maxTemp, maxMinTracker.maxHumidity, -1)
    logSensorReading(maxMinLog, maxMinTracker.minTemp, maxMinTracker.minHumidity, -1)
    maxMinTracker.reset()
    # create yesterday dataset from today
    safeDelete(config.dataDirectory + config.dataFilenameYesterday)
    copyFiles(config.dataDirectory + config.dataFilenameToday, config.dataDirectory + config.dataFilenameYesterday)
    # reset today's data
    safeDelete(config.dataDirectory + config.dataFilenameToday)
    # update historic graphs
    createHistoricGraphSet(config.dataDirectory + config.dataFilenameYesterday,
                           config.dataDirectory + config.dataFilenameRecent,
                           config.dataDirectory + config.dataFilenameMaxMin)


if __name__ == '__main__':

    #                               INITIALISE                                   #

    app.logger.info(getTimestamp() + " Starting thermobot")  # todo add time to log statements
    removeOldGraphFiles()

    if pcMode:
        setConfigToTestValues()
        sensor = temperatureSensorStub()
        config.einkDisplay = False
    else:
        display = Display()
        sensor = temperatureSensor()
        updateEInkWithTempHumidity()
        app.logger.debug(getTimestamp() + "Await clock signal")
        sleep(config.waitForNetworkClock)

    # setup data files
    todayLog = logger(config.dataFilenameToday, config.maxSamplePointsToday, True)
    recentLog = logger(config.dataFilenameRecent, config.maxSamplePointsWeek, True)
    maxMinLog = logger(config.dataFilenameMaxMin, config.maxSamplePointsMaxMin, False)
    lastValueLog = logger(config.dataFilenameLastValue, 1, False)
    maxMinTempHumidityTracker = maxMinTracker()

    # setup webserver
    if config.runWebserver:
        app.logger.debug(getTimestamp() + "Initialise webserver")
        try:
            serverThread = threading.Thread(target=initialise, args=(False,))
            serverThread.start()
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            app.logger.info(message)

    # track (approximately) the time since system start
    clockSeconds = 1
    dayOfMonth = datetime.now().day

    ##############################################################################
    #                               CLOCK CYCLE                                  #
    ##############################################################################

    app.logger.debug(getTimestamp() + "Starting clock cycle")
    while True:

        # record data to file periodically
        if clockSeconds % config.dataSamplePeriodSeconds == 0:
            sensorHumidity, sensorTemperature, logSuccess = getSensorReading(sensor)
            if logSuccess:
                logSensorReading(todayLog, sensorTemperature, sensorHumidity)
                logSensorReading(recentLog, sensorTemperature, sensorHumidity)
                logSensorReading(lastValueLog, sensorTemperature, sensorHumidity)
                maxMinTempHumidityTracker.considerEntry(sensorTemperature, sensorHumidity)
                app.logger.debug(getTimestamp() + "Log recent data to today/recent/lastvalue")
            else:
                app.logger.debug(getTimestamp() + "Sensor reading failed, data not available")

        # generate today's graph periodically through the day
        if clockSeconds % config.updateGraphPeriodSeconds == 0:
            app.logger.debug(getTimestamp() + "Generate graph for today")
            createTodayGraphSet(todayLog.FQFilename)
            # in test, always create the daily graphs too
            if pcMode:
                runEndOfDayActivities(maxMinTempHumidityTracker, maxMinLog)

        # at the start of a new day, regenerate the historic dataset
        if dayOfMonth != datetime.now().day:
            app.logger.debug(getTimestamp() + "Generate historic graphs")
            runEndOfDayActivities(maxMinTempHumidityTracker, maxMinLog)
            dayOfMonth = datetime.now().day

        # generate a slack message periodically
        if config.postToSlack and clockSeconds % config.slackPostPeriodSeconds == 0:
            app.logger.debug(getTimestamp() + "Post to slack")
            updateSlackWithTempHumidity()

        #  update the eInk display with temp and humidity periodically
        if config.einkDisplay and clockSeconds % config.updateDisplayPeriodSeconds == 0:
            app.logger.debug(getTimestamp() + "Update eInk display")
            updateEInkWithTempHumidity()

        # update approximate clock
        sleep(1)
        clockSeconds += 1
