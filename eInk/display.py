import sys
import os
import logging
sys.path.append(os.path.realpath(os.path.dirname(sys.argv[0])) + "/eInk/lib/")
from waveshare_epd import epd2in13bc
from PIL import Image, ImageDraw, ImageFont


class Display:
    def __init__(self):
        picDirectory = os.path.realpath(os.path.dirname(sys.argv[0])) + "/eInk/pic/"
        self.eInkDisplay = epd2in13bc.EPD()
        self.fontMed = ImageFont.truetype(os.path.join(picDirectory, 'Font.ttc'), 13)
        self.fontSmall = ImageFont.truetype(os.path.join(picDirectory, 'Font.ttc'), 8)
        self.fontBig = ImageFont.truetype(os.path.join(picDirectory, 'Font.ttc'), 35)

    def updateDisplay(self, temperature, humidity):
        # format input values
        temperature = "{0:0.1f}c".format(temperature)
        humidity = "{0:0.0f}%".format(humidity)
        # initialise display. Note display size: 0->211 px by 103 px
        self.eInkDisplay.init()
        # hold the black ink elements
        HBlackimage = Image.new('1', (self.eInkDisplay.height, self.eInkDisplay.width), 255)
        drawblack = ImageDraw.Draw(HBlackimage)
        # hold the red ink elements
        HRYimage = Image.new('1', (self.eInkDisplay.height, self.eInkDisplay.width), 255)
        drawry = ImageDraw.Draw(HRYimage)
        # outline of text boxes used in display
        drawblack.rectangle((0, 0, 211, 51), outline=0)
        drawblack.rectangle((0, 51, 211, 103), outline=0)
        # label text
        drawblack.text((10, 0), 'Temperature:', font=self.fontMed, fill=0)
        drawblack.text((10, 50), 'Humidity: ', font=self.fontMed, fill=0)
        # variable text
        drawry.text((110, 0), temperature, font=self.fontBig, fill=0)
        drawry.text((110, 50), humidity, font=self.fontBig, fill=0)
        # footer
        drawblack.rectangle((0, 92, 211, 103), fill=0)
        drawblack.text((10, 93), 'Thermobot5000', font=self.fontSmall, fill=1)
        # push ink layers to the display
        self.eInkDisplay.display(self.eInkDisplay.getbuffer(HBlackimage), self.eInkDisplay.getbuffer(HRYimage))
        # turn off display power
        self.eInkDisplay.sleep()
        return

