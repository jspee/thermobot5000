# Thermobot5000
A temperature and humidity data logger that includes:

* eInk output showing current temperature and humidity levels
* a logged history stored locally as CSV
* a webserver that shows
  * current temperature and humidity levels
  * temperature chart for today
  * temperature chart for yesterday
  * temperature chart of all time

