# values used throughout the application:
# thermometer and/or the webserver
import os
import sys

targetHostname = "raspberrypi"

# directories and datastore
applicationDirectory = os.path.realpath(os.path.dirname(sys.argv[0])) + "/"
dataDirectory = applicationDirectory + "data/"
longTermDataFile = "longTermDataFile.dat"
shortTermDataFile = "shortTermDataFile.dat"
lastValueDataFile = dataDirectory + "lastValue.dat"
websiteContentDirectory = applicationDirectory + "static/"
graphDirectory = websiteContentDirectory + "graphs/"

# samples and data - all in minutes
second = 1
minute = 60 * second
hour = 60 * minute
day = 24 * hour
dataSamplePeriodSeconds = 10 * minute  # (must be > 1) how often a sample is taken
updateGraphPeriodSeconds = 30 * minute
updateDisplayPeriodSeconds = 15 * minute

dataFilenameToday = "today.dat"
maxSamplePointsToday = day

dataFilenameYesterday = "yesterday.dat"
maxSamplePointsYesterday = maxSamplePointsToday

dataFilenameRecent = "recent.dat"  # high resolution data, typically a week
maxSamplePointsWeek = day * 7

dataFilenameMaxMin = "dataMaxMin.dat"  # low resolution data, long term
maxSamplePointsMaxMin = 2 * 365

dataFilenameLastValue = "lastValue.dat"

temperatureCalibration: float = -1

waitForNetworkClock = 1 * minute  # how long the system should wait for a network signal (s)
slackPostPeriodSeconds = 6 * hour

# features
runWebserver = True
postToSlack = False
einkDisplay = True

# webcontent
temperatureLiveBaseFilename = "live_graph_temperature.svg"
humidityLiveBaseFilename = "live_graph_humidity.svg"

temperatureYesterdayBaseFilename = "yesterday_graph_temperature.svg"
humidityYesterdayBaseFilename = "yesterday_graph_humidity.svg"

temperatureRecentBaseFilename = "recent_graph_temperature.svg"
humidityRecentBaseFilename = "recent_graph_humidity.svg"

temperatureMaxMinBaseFilename = "maxmin_graph_temperature.svg"
humidityMaxMinBaseFilename = "maxmin_graph_humidity.svg"

latestTemperatureGraphFilename = "No graph available"

# TEST periods
testDataSamplePeriod = 3  # seconds
testUpdateGraphPeriod = 30  # seconds
testUpdateDisplayPeriod = 30  # seconds
testSlackPostPeriod = 45 * 60  # seconds
