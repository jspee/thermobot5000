import glob
import os
from datetime import datetime, timedelta

import matplotlib.dates as dates
import matplotlib.pyplot as plt
import pandas as pd

import utils.utilities
from config import config



def plotTempHumidityTogether(csvFile, filename):
    headers = ['Date', 'Time', 'Temperature', 'Humidity']
    data = pd.read_csv(csvFile, names=headers, parse_dates=[['Date', 'Time']], dayfirst=True)

    timeSequence = data['Date_Time']
    tempSequence = data['Temperature']
    humiditySequence = data['Humidity']

    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.set_figwidth(8)
    fig.set_figheight(8)
    fig.suptitle("Temperature and Humidity")

    ax1.plot(timeSequence, tempSequence, '-b')
    ax1.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')

    ax2.plot(timeSequence, humiditySequence, '-r')
    ax2.set_xlabel('Date, Time (dd/mm hh)')
    ax2.set_ylabel('Humidity (%)')

    myFmt = dates.DateFormatter('%d/%m %H')
    ax2.xaxis.set_major_formatter(myFmt)

    plt.gcf().autofmt_xdate()

    plt.savefig(filename, dpi=300, pad_inches=1)

    plt.close(fig)


def plotMaxMin(csvFile, dataSeries, filenameStart, graphType):
    # remove the old graph of this type
    removeGraphType(graphType)

    # create a new graph of this type
    filename = filenameStart + graphType
    headers = ['Date', 'Time', 'Temperature', 'Humidity']
    data = pd.read_csv(csvFile, names=headers, parse_dates=['Date'], dayfirst=True)

    dataMax = data.groupby('Date')[dataSeries].max().reset_index()
    dataMin = data.groupby('Date')[dataSeries].min().reset_index()

    dateSequence = dataMax['Date']
    minSequence = dataMin[dataSeries]
    maxSequence = dataMax[dataSeries]

    # bar width
    width = .8

    fig, ax = plt.subplots()

    fig.set_figwidth(8)
    fig.set_figheight(4)
    # get current axes (GCA) and set the y-axis to match the data set
    plt.gca().set_ylim(dataMin[dataSeries].min() - 2, dataMax[dataSeries].max() + 2)

    if dataSeries == 'Temperature':
        ax.bar(dateSequence, maxSequence, width, label='Max', align='center', color='r')
        ax.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')
    else:
        ax.bar(dateSequence, maxSequence, width, label='Max', align='center', color='b')
        ax.set_ylabel('Humidity (%)')

    ax.bar(dateSequence, minSequence, width, label='Min', align='center', color='w')
    ax.xaxis.set_major_formatter(dates.DateFormatter('%d/%m'))
    ax.set_xlabel("day/month")

    plt.gcf().autofmt_xdate()
    plt.savefig(filename, dpi=300, pad_inches=1)

    plt.close(fig)


def plotSeries(csvFile, dataSeries, xAxisLabel, dateTimeFormat, filenameStart, graphType):
    validDatafile = False

    # remove the old graph of this type
    removeGraphType(graphType)

    # check for empty datafile
    if os.path.isfile(csvFile):
        if os.stat(csvFile).st_size != 0:
            validDatafile = True

    if validDatafile:
        # create a new graph of this type
        filename = filenameStart + graphType
        # startDate = (datetime.now() - timedelta(timespan)).strftime("%Y-%m-%d")
        headers = ['Date', 'Time', 'Temperature', 'Humidity']
        data = pd.read_csv(csvFile, names=headers, parse_dates=[['Date', 'Time']], dayfirst=True)
        # dateMask = (data['Date_Time'] > startDate)
        # data = data.loc[dateMask]
        timeSequence = data['Date_Time']
        dataSequence = data[dataSeries]
        fig, ax = plt.subplots()
        fig.set_figwidth(8)
        fig.set_figheight(4)

        # format the graph
        if dataSeries == 'Temperature':
            ax.plot(timeSequence, dataSequence, '-r')
            ax.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')
            ax.set_xlabel(xAxisLabel)
        else:
            ax.plot(timeSequence, dataSequence, '-b')
            ax.set_ylabel('Humidity (%)')
            ax.set_xlabel(xAxisLabel)

        myFmt = dates.DateFormatter(dateTimeFormat)  # '%H:%M'
        ax.xaxis.set_major_formatter(myFmt)
        plt.gcf().autofmt_xdate()

        # save and close
        plt.savefig(filename, dpi=300, pad_inches=1)
        plt.close(fig)


def plotSeriesOld(csvFile, timespan, dataSeries, xAxisLabel, dateTimeFormat, filenameStart, graphType):
    # remove the old graph of this type
    removeGraphType(graphType)

    # create a new graph of this type
    filename = filenameStart + graphType
    startDate = (datetime.now() - timedelta(timespan)).strftime("%Y-%m-%d")
    headers = ['Date', 'Time', 'Temperature', 'Humidity']
    data = pd.read_csv(csvFile, names=headers, parse_dates=[['Date', 'Time']], dayfirst=True)
    dateMask = (data['Date_Time'] > startDate)
    data = data.loc[dateMask]
    timeSequence = data['Date_Time']
    dataSequence = data[dataSeries]
    fig, ax = plt.subplots()
    fig.set_figwidth(8)
    fig.set_figheight(4)

    # format the graph
    if dataSeries == 'Temperature':
        ax.plot(timeSequence, dataSequence, '-r')
        ax.set_ylabel('Temperature (' + u'\N{DEGREE SIGN}' + 'C)')
        ax.set_xlabel(xAxisLabel)
    else:
        ax.plot(timeSequence, dataSequence, '-b')
        ax.set_ylabel('Humidity (%)')
        ax.set_xlabel(xAxisLabel)
    myFmt = dates.DateFormatter(dateTimeFormat)  # '%H:%M'
    ax.xaxis.set_major_formatter(myFmt)
    plt.gcf().autofmt_xdate()

    # save and close
    plt.savefig(filename, dpi=300, pad_inches=1)
    plt.close(fig)


def removeGraphType(graphType: str):
    # Get a list of all the files of graphType
    fileList = glob.glob(config.graphDirectory + "*" + graphType)
    # Iterate over the list of filepaths & remove each file.
    for filePath in fileList:
        utils.utilities.safeDelete(filePath)
